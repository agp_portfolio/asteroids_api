import { HttpStatusCode } from "axios"
import express, { Request, Response, Router } from "express"

const router: Router = express.Router()

router.use("/health", (req: Request, res: Response) => {
  return res.status(HttpStatusCode.Ok).json("The server is up and running")
})

router.use("/asteroids", require("./asteroids.routes"))

router.use("/fav_asteroids", require("./fav_asteroids.routes"))

module.exports = router
