import express, { Router } from "express"
import controller from "../controllers/asteroids"
import validators from "../validators/asteroids"

const router: Router = express.Router()

router.get(
  "/bypage/:page",
  validators.validateQueryParamsPage,
  controller.getAsteroidsPage
)

router.get(
  "/bydate/:start_date/:end_date",
  validators.validateQueryParamsDates,
  controller.getAsteroidsByDates
)


export = router
