import express, { Router } from "express"
import controller from "../controllers/fav_asteroids"
import validators from "../validators/fav_asteroids"

const router: Router = express.Router()

router.get(
  "",
  controller.getFavAsteroids
)

router.post(
  "",
  validators.validateBodyAsteroid,
  controller.addFavAsteroids
)

router.delete(
  "/:id",
  validators.validateQueryParamsId,
  controller.removeFavAsteroids
)


export = router
