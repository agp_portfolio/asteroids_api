export interface Asteroid {
  id: string
  name: string
  url: string
  isFavorite: boolean
}

export interface ListAsteroidResponse {
  page: number
  nextPage?: number
  prevPage?: number
  totalPages: number
  size: number
  totalElements: number
  asteroids: Asteroid[]
}
