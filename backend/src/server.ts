import app from "./app"
import logger from "./logger"

// Start server
app.listen(app.get("port"), async () => {
  logger.info(`asteroids_backend running on port ${app.get("port")}.`)
})
