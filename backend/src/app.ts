import express, { Express } from "express"
import cors from "cors"
import morgan from "morgan"

import bodyParser from "body-parser"

require("dotenv").config()

const app: Express = express()

app.set("port", 4000)

// Middlewares
app.use(cors())
app.use(morgan("dev"))
app.use(bodyParser.json())

// Routes
app.use("/", require("./routes/index.routes"))

export default app
