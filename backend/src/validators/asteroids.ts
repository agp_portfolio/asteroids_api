import { HttpStatusCode } from "axios"
import { Request, Response, NextFunction } from "express"
import Joi, { ValidationResult } from "joi"
import logger from "../logger"
import { ParamsDictionary } from "express-serve-static-core"

const validateQueryParamsPage = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { params }: { params: ParamsDictionary } = req
  // Needed an object with page property
  const schema = Joi.object()
    .keys({
      page: Joi.number().required()
    })
    .required()

  const result: ValidationResult = schema.validate(params)

  const { error } = result

  const valid: boolean = error == null

  if (!valid) {
    logger.error("Validation error: Params page")
    res.status(HttpStatusCode.BadRequest).json({
      message: "Invalid request"
    })
  } else {
    next()
  }
}

const validateQueryParamsDates = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { params }: { params: ParamsDictionary } = req
  // Needed an object with page property
  const schema = Joi.object()
    .keys({
      start_date: Joi.date().required(),
      end_date: Joi.date().required()
    })
    .required()

  const result: ValidationResult = schema.validate(params)

  const { error } = result
  const valid: boolean = error == null

  if (!valid) {
    logger.error("Validation error: Params date")
    res.status(HttpStatusCode.BadRequest).json({
      message: "Invalid request"
    })
  } else {
    next()
  }
}

export default {
  validateQueryParamsPage,
  validateQueryParamsDates
}
