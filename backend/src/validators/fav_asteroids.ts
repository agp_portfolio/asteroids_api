import { HttpStatusCode } from "axios"
import { Request, Response, NextFunction } from "express"
import Joi, { ValidationResult } from "joi"
import logger from "../logger"
import { ParamsDictionary } from "express-serve-static-core"
import { Asteroid } from "../models/asteroid"

const validateQueryParamsId = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { params }: { params: ParamsDictionary } = req
  // Needed an object with id property
  const schema = Joi.object()
    .keys({
      id: Joi.number().required()
    })
    .required()

  const result: ValidationResult = schema.validate(params)

  const { error } = result

  const valid: boolean = error == null

  if (!valid) {
    logger.error("Validation error: Params id")
    res.status(HttpStatusCode.BadRequest).json({
      message: "Invalid request"
    })
  } else {
    next()
  }
}

const validateBodyAsteroid = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { body }: { body: Asteroid } = req
  // Needed an object with the asteroid info
  const schema = Joi.object()
    .keys({
      id: Joi.string().required(),
      name: Joi.string().required(),
      url: Joi.string().uri().required(),
      isFavorite: Joi.boolean()
    })
    .required()

  const result: ValidationResult = schema.validate(body)

  const { error } = result

  const valid: boolean = error == null

  if (!valid) {
    logger.error("Validation error: Body asteroid")
    res.status(HttpStatusCode.BadRequest).json({
      message: "Invalid request"
    })
  } else {
    next()
  }
}

export default {
  validateQueryParamsId,
  validateBodyAsteroid
}
