import { createLogger, transports, format } from "winston"

const logger = createLogger({
  level: "info",
  format: format.combine(
    format.timestamp(),
    format.json(),
    format.printf(info => `[asteroids_backend] ${info.timestamp} ${info.level}: ${info.message}`)
  ),
  defaultMeta: { service: "asteroids_backend" },
  transports: [
    new transports.Console(),
    new transports.File({ filename: "./logs/asteroids_backend.log" })
  ]
})

export default logger
