import { HttpStatusCode } from "axios"
import { Request, Response } from "express"
import logger from "../logger"
import FavAsteroidsServices from "../services/fav_asteroids"

// Get all of the favorite asteroids
const getFavAsteroids = async (req: Request, res: Response) => {
  try {
    const results = await FavAsteroidsServices.getFavAsteroids()

    res.status(HttpStatusCode.Ok).json(results)
  } catch (error) {
    logger.error(error)
    return res.status(HttpStatusCode.InternalServerError).json({
      message: "Server error"
    })
  }
}

// Add a favorite asteroid
const addFavAsteroids = async (req: Request, res: Response) => {
  try {
    const results = await FavAsteroidsServices.addFavAsteroids(req.body)

    res.status(HttpStatusCode.Created).json(results)
  } catch (error) {
    logger.error(error)
    return res.status(HttpStatusCode.InternalServerError).json({
      message: "Server error"
    })
  }
}

// Remove a favorite asteroid
const removeFavAsteroids = async (req: Request, res: Response) => {
  try {
    const results = await FavAsteroidsServices.removeFavAsteroids(req.params)

    res.status(HttpStatusCode.Ok).json(results)
  } catch (error) {
    logger.error(error)
    return res.status(HttpStatusCode.InternalServerError).json({
      message: "Server error"
    })
  }
}



export default {
  getFavAsteroids,
  addFavAsteroids,
  removeFavAsteroids
}
