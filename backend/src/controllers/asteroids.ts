import { HttpStatusCode } from "axios"
import { Request, Response } from "express"
import logger from "../logger"
import AsteroidsServices from "../services/asteroids"

// Get list of asteroids by page
const getAsteroidsPage = async (req: Request, res: Response) => {
  try {
    const results = await AsteroidsServices.getAsteroidsPage(req.params)

    res.status(HttpStatusCode.Ok).json(results)
  } catch (error) {
    logger.error(error)
    return res.status(HttpStatusCode.InternalServerError).json({
      message: error
    })
  }
}

// Get list of asteroids by dates
const getAsteroidsByDates = async (req: Request, res: Response) => {
  try {
    const results = await AsteroidsServices.getAsteroidsByDates(req.params)

    res.status(HttpStatusCode.Ok).json(results)
  } catch (error) {
    logger.error(error)
    return res.status(HttpStatusCode.InternalServerError).json({
      message: error
    })
  }
}

export default {
  getAsteroidsPage,
  getAsteroidsByDates
}
