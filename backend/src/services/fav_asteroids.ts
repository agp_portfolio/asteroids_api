import { ParamsDictionary } from "express-serve-static-core"
import { Asteroid, ListAsteroidResponse } from "../models/asteroid"
// The favorite asteroids data should be in a database but it has been done this way for simplicty
import { favAsteroids } from "../data/fav_asterorids"

// Get the list of favorite asteroids
const getFavAsteroids = async () => {
  const processedResult: ListAsteroidResponse = {
    page: 0,
    totalElements: favAsteroids.length,
    totalPages: 1,
    size: favAsteroids.length,
    asteroids: favAsteroids
  }
  return processedResult
}

// Add an asteroid to the favorite asteroids list
const addFavAsteroids = async (asteroid: Asteroid) => {
  const objectExists = favAsteroids.some(
    (obj: Asteroid) => obj.id === asteroid.id
  )
  if (!objectExists) {
    favAsteroids.push({ ...asteroid, isFavorite: true })
    return asteroid.id
  }
  return
}

// Remove an asteroid from the favorite asteroids list
const removeFavAsteroids = async (params: ParamsDictionary) => {
  const id: string = params.id
  const index = favAsteroids.findIndex((obj: Asteroid) => obj.id === id)
  if (index !== -1) {
    favAsteroids.splice(index, 1)
    return id
  }

  return
}

export default {
  getFavAsteroids,
  addFavAsteroids,
  removeFavAsteroids
}
