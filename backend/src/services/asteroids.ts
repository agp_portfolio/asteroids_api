import { ParamsDictionary } from "express-serve-static-core"
import logger from "../logger"
import axios, { AxiosResponse } from "axios"
import { Asteroid, ListAsteroidResponse } from "../models/asteroid"
import moment from "moment"
import { favAsteroids } from "../data/fav_asterorids"

// Get list of asteroids by page
const getAsteroidsPage = async (params: ParamsDictionary) => {
  const page: number = parseInt(params.page)

  try {
    const result: AxiosResponse = await axios.get(
      `https://api.nasa.gov/neo/rest/v1/neo/browse?page=${page}&size=10&api_key=${process.env.API_KEY}`
    )

    const processedResult: ListAsteroidResponse = {
      page: page,
      totalElements: result.data.page.total_elements,
      totalPages: result.data.page.total_pages,
      size: result.data.page.size,
      asteroids: []
    }

    if (result.data.links.next) processedResult.nextPage = page + 1
    if (result.data.links.prev) processedResult.prevPage = page - 1

    processedResult.asteroids = result.data.near_earth_objects.map(
      (info: any) => {
        return {
          id: info.id,
          name: info.name,
          url: info.nasa_jpl_url,
          isFavorite: favAsteroids.some((obj: Asteroid) => obj.id === info.id)
        }
      }
    )

    return processedResult
  } catch (error) {
    logger.error("Coudn't get the info from the api")
    return
  }
}

// Get list of asteroids by dates
const getAsteroidsByDates = async (params: ParamsDictionary) => {
  const startDate: string = moment(params.start_date).format("YYYY-MM-DD")
  const endDate: string = moment(params.end_date).format("YYYY-MM-DD")
  try {
    const result: AxiosResponse = await axios.get(
      `https://api.nasa.gov/neo/rest/v1/feed?start_date=${startDate}&end_date=${endDate}&api_key=${process.env.API_KEY}`
    )
    const processedResult: ListAsteroidResponse = {
      page: 0,
      totalElements: result.data.element_count,
      totalPages: 1,
      size: result.data.element_count,
      asteroids: []
    }

    const tempAsteroids: Asteroid[] = []
    for (const date in result.data.near_earth_objects) {
      result.data.near_earth_objects[date].forEach((info: any) => {
        const asteroid: Asteroid = {
          id: info.id,
          name: info.name,
          url: info.nasa_jpl_url,
          isFavorite: favAsteroids.some((obj: Asteroid) => obj.id === info.id)
        }
        const exist = tempAsteroids.some(
          (asteroid: Asteroid) => asteroid.id == info.id
        )
        if (!exist) {
          tempAsteroids.push(asteroid)
        }
      })
    }
    processedResult.totalElements = tempAsteroids.length
    processedResult.size = tempAsteroids.length

    processedResult.asteroids = tempAsteroids.sort(
      (a: Asteroid, b: Asteroid) => {
        if (a.name < b.name) {
          return -1
        }
        if (a.name > b.name) {
          return 1
        }
        return 0
      }
    )

    return processedResult
  } catch (error) {
    logger.error("Coudn't get the info from the api")
    return
  }
}

export default {
  getAsteroidsPage,
  getAsteroidsByDates
}
