/* eslint-disable no-undef */
import request from "supertest"
import app from "../../../app"
import { Asteroid } from "../../../models/asteroid"

describe("### GET /fav_asteroids", () => {
  test("should return 200", async () => {
    const response = await request(app).get("/fav_asteroids").expect(200)

    if (Array.isArray(response.body) && response.body.length > 0) {
      expect(response.body).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            id: expect.any(String),
            name: expect.any(String),
            url: expect.any(String)
          })
        ])
      )
    } else {
      expect(response.body).toEqual([])
    }
  })
})

describe("### POST /fav_asteroids", () => {
  test("should return 200", async () => {
    const asteroid: Asteroid = {
      id: "1234",
      name: "asdf",
      url: "https://www.ejemplo.com",
      isFavorite: true
    }
    const response = await request(app)
      .post("/fav_asteroids")
      .send(asteroid)
      .expect(201)
    expect(response.body).toEqual(asteroid.id)
  })
})

describe("### DELETE /fav_asteroids/id", () => {
  test("should return 200", async () => {
    const id = "1234"
    const response = await request(app)
      .delete(`/fav_asteroids/${id}`)
      .expect(200)
    expect(response.body).toEqual(id)
  })
})
