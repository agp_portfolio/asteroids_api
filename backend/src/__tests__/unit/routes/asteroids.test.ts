/* eslint-disable no-undef */
import request from "supertest"
import app from "../../../app"

describe("### GET /asteroids/bypage/:page", () => {
  test("should return 200", async () => {
    let page = 0
    const response = await request(app)
      .get(`/asteroids/bypage/${page}`)
      .expect(200)

    expect(response.body.page).toEqual(page)
    expect(response.body.nextPage).toEqual(page + 1)
    expect(response.body.asteroids).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          id: expect.any(String),
          name: expect.any(String),
          url: expect.any(String)
        })
      ])
    )

    page += 1

    const response2 = await request(app)
      .get(`/asteroids/bypage/${page}`)
      .expect(200)

    expect(response2.body.prevPage).toEqual(page - 1)
    expect(response2.body.page).toEqual(page)
    expect(response2.body.nextPage).toEqual(page + 1)
    expect(response2.body.asteroids).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          id: expect.any(String),
          name: expect.any(String),
          url: expect.any(String)
        })
      ])
    )
  })
})

describe("### GET /asteroids/bydate/:start_date/:end_date", () => {
  test("should return 200", async () => {
    const start_date = "2015-09-07"
    const end_date = "2015-09-08"
    const response = await request(app)
      .get(`/asteroids/bydate/${start_date}/${end_date}`)
      .send()
      .expect(200)

    expect(response.body.page).toEqual(0)
    expect(response.body.asteroids).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          id: expect.any(String),
          name: expect.any(String)
        })
      ])
    )
  })
})
