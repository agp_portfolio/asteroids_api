import { Asteroid } from "../models/asteroid";

export const useGetFavAsteroids = () => {
  return fetch(`http://localhost:4000/fav_asteroids`, {
    method: "GET",
    mode: "cors",
    headers: {
      "Content-type": "application/json",
    },
  })
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      alert(err);
    });
};


export const useSetFavAsteroid = (asteroid:Asteroid) => {
  return fetch(
    `http://localhost:4000/fav_asteroids`,
    {
      method: "POST",
      mode: "cors",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(asteroid)
    }
  )
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      alert(err);
    });
};

export const useUnsetFavAsteroid = (id:string) => {
  return fetch(
    `http://localhost:4000/fav_asteroids/${id}`,
    {
      method: "DELETE",
      mode: "cors",
      headers: {
        "Content-type": "application/json",
      }
    }
  )
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      alert(err);
    });
};
