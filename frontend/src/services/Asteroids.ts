export const useGetAsteroidsByPage = (page: number) => {
    return fetch(`http://localhost:4000/asteroids/bypage/${page}`, {
      method: "GET",
      mode: "cors",
      headers: {
        "Content-type": "application/json",
      },
    })
      .then((response) => {
        return response.json();
      })
      .catch((err) => {
        alert(err);
      });
  };

  export const useGetAsteroidsByDate = (startDate: string, endDate: string) => {
    return fetch(`http://localhost:4000/asteroids/bydate/${startDate}/${endDate}`, {
      method: "GET",
      mode: "cors",
      headers: {
        "Content-type": "application/json",
      },
    })
      .then((response) => {
        return response.json();
      })
      .catch((err) => {
        alert(err);
      });
  };
  