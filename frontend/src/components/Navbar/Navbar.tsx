import React, { FC } from "react";
import { Link } from "react-router-dom";
const Navbar: FC = () => {
  return (
    <div className="navbar">
      <li>
        <Link to="/">Lista Asteroides</Link>
      </li>
      <li>
        <Link to="/date">Asteroides por fecha</Link>
      </li>
      <li>
        <Link to="/fav">Asteroides favoritos</Link>
      </li>
    </div>
  );
};
export default Navbar;
