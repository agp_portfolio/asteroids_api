import React, { FC } from "react";
import { BiStar, BiLink } from "react-icons/bi";
import { BsStarFill } from "react-icons/bs";
import { Asteroid, ListAsteroidResponse } from "../../models/asteroid";
import {
  useSetFavAsteroid,
  useUnsetFavAsteroid,
} from "../../services/FavAsteroids";

interface TableProps {
  listAsteroidResponse: ListAsteroidResponse;
  onPageChange: (page: number) => void;
  updatePage: () => void;
}

const Table: FC<TableProps> = (props: TableProps) => {
  const { listAsteroidResponse, onPageChange, updatePage } = props;
  const { page, nextPage, prevPage, totalPages } = listAsteroidResponse;
  const setFavorite = async(asteroid: Asteroid) => {
    if (asteroid.isFavorite) {
      // eslint-disable-next-line react-hooks/rules-of-hooks
      await useUnsetFavAsteroid(asteroid.id);
    } else {
      // eslint-disable-next-line react-hooks/rules-of-hooks
      await useSetFavAsteroid(asteroid);
    }
    updatePage();
  };
  return (
    <div>
      <table className="asteroids-table">
        <thead>
          <tr>
            {/* <th>Id</th> */}
            <th>Fav</th>
            <th style={{ width: "100%" }}>Nombre</th>
            <th>Link</th>
          </tr>
        </thead>
        <tbody>
          {listAsteroidResponse.asteroids?.map((item: Asteroid) => (
            <tr key={item.id}>
              {/* <td>{item.id}</td> */}
              <td>
                <span onClick={() => setFavorite(item)}>
                  {item.isFavorite ? <BsStarFill /> : <BiStar />}
                </span>
              </td>
              <td>{item.name}</td>
              <td>
                <a href={item.url} target="_blank" rel="noopener noreferrer">
                  <BiLink />
                </a>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      {(prevPage || nextPage) && (
        <div className="pagination">
          {prevPage !== undefined &&  (
            <button onClick={() => onPageChange(prevPage)}>Anterior</button>
          )}
          <p>
            Página {page + 1} de {totalPages}
          </p>
          {nextPage && (
            <button onClick={() => onPageChange(nextPage)}>Siguiente</button>
          )}
        </div>
      )}
    </div>
  );
};

export default Table;
