import React from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate } from "react-router-dom";
import './App.css';
import Asteroids from './pages/Asteroids';
import FavAsteroids from './pages/FavAsteroids';
import DateAsteroids from './pages/DateAsteroids';
import Navbar from './components/Navbar/Navbar';

function App() {
  return (
    <Router>
      <Navbar />
      <Routes>
        <Route path="/" element={<Asteroids />} />
        <Route path="/fav" element={<FavAsteroids />} />
        <Route path="/date" element={<DateAsteroids />} />


        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
    </Router>
  );
}

export default App;
