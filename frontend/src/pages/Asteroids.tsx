import React, { useState, useEffect } from "react";

import { ListAsteroidResponse } from "../models/asteroid";
import Table from "../components/Table/Table";
import { useGetAsteroidsByPage } from "../services/Asteroids";

const Asteroids: React.FC = () => {
  const [listAsteroidResponse, setListAsteroidResponse] =
    useState<ListAsteroidResponse>();
  const [page, setPage] = useState<number>(0);
  const [updateTable, setUpdateTable] = React.useState<number>(0);

  useEffect(() => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    UseGetAsteroidsByPageAction();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [updateTable]);

  const UseGetAsteroidsByPageAction = () => {
    useGetAsteroidsByPage(page).then((response: ListAsteroidResponse) => {
      setListAsteroidResponse(response);
      console.log(response);
    });
  };

  const onPageChange = (page: number) => {
    setPage(page);
    updatePage();
  };

  const updatePage = () => {
    setUpdateTable(updateTable + 1);
  };

  return (
    <div>
      <h1 className="title">Asteroides</h1>
      <div>
        {listAsteroidResponse ? (
          <Table
            listAsteroidResponse={listAsteroidResponse}
            onPageChange={onPageChange}
            updatePage={updatePage}
          />
        ):<div>Loading...</div>}
      </div>
    </div>
  );
};

export default Asteroids;
