import React, { useState, useEffect } from "react";

import { ListAsteroidResponse } from "../models/asteroid";
import Table from "../components/Table/Table";
import { useGetFavAsteroids } from "../services/FavAsteroids";

const FavAsteroids: React.FC = () => {
  const [listAsteroidResponse, setListAsteroidResponse] =
    useState<ListAsteroidResponse>();
  const [updateTable, setUpdateTable] = React.useState<number>(0);

  useEffect(() => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    UseGetAsteroidsByPageAction();
  }, [updateTable]);

  const UseGetAsteroidsByPageAction = () => {
    useGetFavAsteroids().then((response: ListAsteroidResponse) => {
      setListAsteroidResponse(response);
    });
  };
  
  const updatePage = () => {
    setUpdateTable(updateTable + 1);
  };

  return (
    <div>
      <h1 className="title">Asteroides favoritos</h1>
      {listAsteroidResponse && (
        <Table
          listAsteroidResponse={listAsteroidResponse}
          onPageChange={() => {}}
          updatePage={updatePage}
        />
      )}
    </div>
  );
};

export default FavAsteroids;
