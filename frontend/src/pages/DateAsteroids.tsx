import React, { useState, useEffect } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { ListAsteroidResponse } from "../models/asteroid";
import Table from "../components/Table/Table";
import { useGetAsteroidsByDate } from "../services/Asteroids";

const DateAsteroids: React.FC = () => {
  const [listAsteroidResponse, setListAsteroidResponse] =
    useState<ListAsteroidResponse>();
  const [updateTable, setUpdateTable] = React.useState<number>(0);
  const [startDate, setStartDate] = useState<Date>(new Date());
  const [endDate, setEndDate] = useState<Date>(new Date());

  useEffect(() => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    UseGetAsteroidsByPageAction();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [updateTable]);

  const UseGetAsteroidsByPageAction = () => {
    var start = startDate.toISOString().split("T")[0];
    var end = endDate.toISOString().split("T")[0];
    console.log(start, end);
    useGetAsteroidsByDate(start, end).then((response: ListAsteroidResponse) => {
      setListAsteroidResponse(response);
    });
  };

  const onChangeStartDate = (date: Date) => {
    setStartDate(date);
    updatePage();
  };
  const onChangeEndDate = (date: Date) => {
    setEndDate(date);
    updatePage();
  };

  const updatePage = () => {
    setUpdateTable(updateTable + 1);
  };

  return (
    <div>
      <h1 className="title">Asteroides por fecha</h1>

      {/* Agrega los campos de selección de fecha */}
      <div className="dates">
        <div>
          <label htmlFor="startDate">Fecha de inicio:</label>
          <DatePicker
            id="startDate"
            selected={startDate}
            onChange={onChangeStartDate}
            dateFormat="dd/MM/yyyy"
          />
        </div>
        <div>
          <label htmlFor="endDate">Fecha de fin:</label>
          <DatePicker
            id="endDate"
            selected={endDate}
            onChange={onChangeEndDate}
            dateFormat="dd/MM/yyyy"
          />
        </div>
      </div>

      {listAsteroidResponse ? (
        <Table
          listAsteroidResponse={listAsteroidResponse}
          onPageChange={() => {}}
          updatePage={updatePage}
        />
      ) : (
        <div>Loading...</div>
      )}
    </div>
  );
};

export default DateAsteroids;
